#!/bin/bash
rm class_uam_iv.mod
rm cf2cmu
ifort class_uam_iv.f90 cf2cmu.f90 -o cf2cmu -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source
# ifort class_ptfile.f90 cf2cmu.f90 -o cf2cmu -O2 -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

rm area.pmcamx.20130712.bin point.pmcamx.20130712.bin

./cf2cmu << EOF
area.camx.20130712.bin
area.pmcamx.20130712.bin
EOF

./cf2cmu << EOF
point.camx.20130712.bin
point.pmcamx.20130712.bin
EOF
