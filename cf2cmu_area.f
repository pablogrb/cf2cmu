      program convertcamxareaemisstopmcamx
c
c    File below used to redistribute: 2008 CAMx base area emissions 
c                                     to PMCAMx-friendly emissions
c                                     with Robinson IVOCs
c    Note: compile on Moe not Barney. Barney still does not match
c          species names correctly (compiler compatibility issue?)
c    - Laura Posner (08/04/2015) laura.n.posner@gmail.com
c
      implicit none

      integer ifin,ifout,MXCOLA,MXROWA,MXLAYA,MXSPEC,nspecnew,nday

      parameter(ifin = 10) !CAMx emission file unit number
      parameter(ifout = 20) !PMCAMx emission file unit number
      parameter(MXCOLA = 200) !number of grid columns guess
      parameter(MXROWA = 200) !number of grid rows guess
      parameter(MXLAYA = 20) !number of grid layers guess
      parameter(MXSPEC = 100) !number of PMCAMx species guess
      parameter(nspecnew = 88) !exact number of species in nspclist below
      parameter(nday = 30) !exact number of days of files to process

      character*200     fin, indir, fout, outdir, fendin, fendout
      character*40      tmspcname(nspecnew),tmspcname2(nspecnew)
      character*32      spacingarray(9),spacing
      character*4       mspec(10,MXSPEC), mspecnew(10,nspecnew)
      character*10      newspclist(nspecnew),mspecunspaced(MXSPEC)
      character*4       newspc(10,nspecnew)
      character*4       name(10),note(60)
      character*2       month,dates(nday)

      integer     len,i,j,xgrd,ygrd,k2,isp,n,l,mm,len2,lold,lnew,t
      integer     ione,nspec,ibdate,iedate,iutm,nx,ny,nz,idum,idate

      real        btime,etime,rdum,xorg,yorg,delx,dely
      real        aremis(MXCOLA,MXROWA,MXSPEC)
      real        aremisnew(MXCOLA,MXROWA,nspecnew)
      real        apofactorlist(8),cpofactorlist(8),cnsfactorlist(8)
      real        apofactor, cpofactor, cnsfactor
      real        MW, pm25splitfactor, pmcoarsesplitfactor

      month = '04'

      data dates /'01', '02', '03', '04', '05', '06', '07', '08', '09',
     &            '10', '11', '12', '13', '14', '15', '16', '17', '18', 
     &            '19', '20', '21', '22', '23', '24', '25', '26', '27', 
     &            '28', '29', '30'/

      data indir /'/Users/Inputs/ENVIRON_2008/ENVIRON_original_files/Not_PMCAMx_ready/emiss/model-ready/low/'/
      data fendin /'camx.ar.westjump.2008c1-all.cb05.conus36km.cf.2008'/
      data outdir /'/Users/Inputs/ENVIRON_2008/PMCAMx_files/emiss/BASE/'/
      data fendout /'pmcamx.ar.westjump.2008c1-all.cb05.conus36km.cf.2008'/

      data apofactorlist /0.09, 0.09, 0.14, 0.18, 0.0, 0.0, 0.0, 0.0/
      data cpofactorlist /0.0,  0.0,  0.0,  0.0,  0.3, 0.2, 0.0, 0.0/
      data cnsfactorlist /0.0,  0.0,  0.0,  0.0,  0.0, 0.2, 0.5, 0.8/

      MW = 250.
      pm25splitfactor = 1./6.
      pmcoarsesplitfactor = 1./2.

      data newspclist /'NO', 'NO2',     'HONO',    'CO',      'OLE',
     &            'PAR',     'TOL',     'XYL',     'FORM',    'ALD2',
     &            'ETH',     'MEOH',    'ETOH',    'ISOP',    'IOLE',
     &            'TERP',    'ALDX',    'CH4',     'ETHA',    'SO2',
     &            'SULF',    'NH3',
     &            'PSO4_1',  'PSO4_2',  'PSO4_3',  'PSO4_4',  'PSO4_5',
     &            'PSO4_6',
     &            'PNO3_1',  'PNO3_2',  'PNO3_3',  'PNO3_4',  'PNO3_5',
     &            'PNO3_6',
     &            'CRST_7',  'CRST_8',
     &            'CRST_1',  'CRST_2',  'CRST_3',  'CRST_4',  'CRST_5',
     &            'CRST_6',
     &            'PEC_1',   'PEC_2',   'PEC_3',   'PEC_4',   'PEC_5',
     &            'PEC_6',
     &            'APO1_1',  'APO1_2',  'APO1_3',  'APO1_4',  'APO1_5',
     &            'APO1_6',
     &            'APO2_1',  'APO2_2',  'APO2_3',  'APO2_4',  'APO2_5',
     &            'APO2_6',
     &            'APO3_1',  'APO3_2',  'APO3_3',  'APO3_4',  'APO3_5',
     &            'APO3_6',
     &            'APO4_1',  'APO4_2',  'APO4_3',  'APO4_4',  'APO4_5',
     &            'APO4_6',
     &            'CPO5',    'CPO6',    'CNS6',    'CNS7',    'CNS8',
     &            'ISP',     'TRP',     'SQT',     'AACD',    'FACD',
     &            'ISPD',    'CRES',    'OPEN',    'MGLY',    'TOLA',
     &            'XYLA'/

c Create species name for PMCAMx-UF
      data spacingarray /'    ',                                      !1
     &                   '        ',                                  !2
     &                   '            ',                              !3
     &                   '                ',                          !4
     &                   '                    ',                      !5
     &                   '                        ',                  !6
     &                   '                            ',              !7
     &                   '                                ',          !8
     &                   '                                    '/      !9

      do l = 1, nspecnew
         tmspcname(l) = ''
      enddo !l

      do l=1,nspecnew
         len = INDEX(newspclist(l),' ')
         spacing = spacingarray(10-len-1)
         if (len.eq.3) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//spacing(1:32)
         elseif (len.eq.4) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//newspclist(l)(3:3)//'   '//spacing(1:28)
         elseif (len.eq.5) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//newspclist(l)(3:3)//'   '//
     &       newspclist(l)(4:4)//'   '//spacing(1:24)
         elseif (len.eq.6) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//newspclist(l)(3:3)//'   '//
     &       newspclist(l)(4:4)//'   '//newspclist(l)(5:5)//'   '//spacing(1:20)
         elseif (len.eq.7) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//newspclist(l)(3:3)//'   '//
     &       newspclist(l)(4:4)//'   '//newspclist(l)(5:5)//'   '//newspclist(l)(6:6)//'   '//spacing(1:16)
         elseif (len.eq.8) then
             tmspcname(l) = newspclist(l)(1:1)//'   '//newspclist(l)(2:2)//'   '//newspclist(l)(3:3)//'   '//
     &       newspclist(l)(4:4)//'   '//newspclist(l)(5:5)//'   '//newspclist(l)(6:6)//'   '//newspclist(l)(7:7)
     &       //'   '//spacing(1:12)
         endif !len

         mspecnew(1,l) = tmspcname(l)(1:4)
         mspecnew(2,l) = tmspcname(l)(5:8)
         mspecnew(3,l) = tmspcname(l)(9:12)
         mspecnew(4,l) = tmspcname(l)(13:16)
         mspecnew(5,l) = tmspcname(l)(17:20)
         mspecnew(6,l) = tmspcname(l)(21:24)
         mspecnew(7,l) = tmspcname(l)(25:28)
         mspecnew(8,l) = tmspcname(l)(29:32)
         mspecnew(9,l) = tmspcname(l)(33:36)
         mspecnew(10,l) = tmspcname(l)(37:40)

      enddo !l

      do idate = 1,nday

c set input & output file names

       len = INDEX(indir,' ')
       fin = indir(1:len-1)//fendin
       len = INDEX(fin,' ')
       fin = fin(1:len-1)//month(1:2)//dates(idate)
       write (*,*) 'input file name: ', fin

       len = INDEX(outdir,' ')
       fout = outdir(1:len-1)//fendout
       len = INDEX(fout,' ')
       fout = fout(1:len-1)//month(1:2)//dates(idate)
       write (*,*) 'output file name: ', fout

c
c open output file if necessary
c
      len = INDEX(fout,' ')
      if(ifout.ne.6) open(unit=ifout,file=fout(1:len-1),status='unknown'
     &                ,form='UNFORMATTED')

c
c open input files
c
      len = INDEX(fin,' ')
      open(unit=ifin,file=fin(1:len-1),form='UNFORMATTED',status='old')


c    Header 1 name,note,ione,nspec,ibdate,btime,iedate,etime
      read(ifin) name,note,ione,nspec,ibdate,btime,iedate,etime
      write(ifout) name,note,ione,nspecnew,ibdate,btime,iedate,etime
      write(*,*) 'All from: ', fin, ' name: ', name,', note: ', note, ', ione: ', ione, ', nspec: ', nspec,
     &           ', ibdate: ',ibdate, ', btime: ', btime, ', iedate: ', iedate,', etime: ', etime


c     Header 2 rdum,rdum,iutm,xorg,yorg,delx,dely,nx,ny,nz,idum,idum,rdum
      read(ifin) rdum,rdum,nz,xorg,yorg,delx,dely,nx,ny,iutm,idum,idum
     &           ,rdum,rdum,rdum
      write(ifout) rdum,rdum,iutm,xorg,yorg,delx,dely,nx,ny,nz,idum,idum
     &           ,rdum,rdum,rdum
      write (*,*) 'All from: ', fin, ', rdum: ', rdum,', iutm: ', iutm, ', xorg: ', xorg,', yorg: ', yorg,
     &            ', delx: ', delx, ', dely: ', dely, ', nx: ', nx,', ny: ', ny,
     &            ', nz: ', nz, ', idum: ', idum, ', rdum: ', rdum


c     Header 3 ione,ione,nx,ny
      read(ifin) ione,ione,nx,ny
      write(ifout) ione,ione,nx,ny
      write(*,*) 'All from: ', fin, ', ione: ',ione, ', nx: ', nx,', ny: ', ny


c     Header 4 (mspec(l),l=1,nspec)
      read(ifin) ((mspec(n,l),n=1,10),l=1,nspec)
      write(ifout) ((mspecnew(n,l),n=1,10),l=1,nspecnew)
      write(*,*), 'All from: ', fin, ((mspec(n,l)(1:1),n=1,10),l=1,nspec)

c
c read & write time-variant portion
c
      t = 0

 100  read(ifin,end=900) ibdate,btime,iedate,etime
      write(ifout) ibdate,btime,iedate,etime
      t = t + 1

c Clear Sums and Concentrations
      do l = 1, MXSPEC
       do i = 1, nx
          do j = 1, ny
             aremis(i,j,l) = 0.
          enddo !j
       enddo !i
      enddo!l

      do l = 1, nspecnew
       do i = 1, nx
          do j = 1, ny
             aremisnew(i,j,l) = 0.
          enddo !j
       enddo !i
      enddo!l


c Read input file for time specified

      do l = 1, nspec
         read(ifin) ione, (mspec(n,l),n=1,10),
     &      ((aremis(i,j,l),i=1,nx),j=1,ny)
         write (mspecunspaced(l),*), (mspec(n,l)(1:1),n=1,10)
      enddo !l


c redistribution
      do lnew = 1, nspecnew
         len2 = INDEX(newspclist(lnew),'_')
         do lold = 1, nspec
            len = INDEX(mspecunspaced(lold),' ')

c  gases
           if ( (newspclist(lnew).eq.mspecunspaced(lold)(1:len-1)) .and.
     &           (INDEX(newspclist(lnew),'_').eq.0) ) then
                 if (t.eq.1) write(*,*) 'Match found of gas CAMx species: ', mspecunspaced(lold),
     &                      ' and PMCAMx species: ', newspclist(lnew)
                 do i = 1,nx
                    do j = 1,ny
                       aremisnew(i,j,lnew) = aremis(i,j,lold)
                    enddo !j
                 enddo !i

c  aerosol besides CRST and organics
           elseif
     &        ( (newspclist(lnew)(1:len2-1).eq.mspecunspaced(lold)(1:len-1))
     &        .and. (INDEX(newspclist(lnew),'_').ne.0)              
     &        .and. (newspclist(lnew)(1:len2-1).ne.'CRST') ) then
                 if (t.eq.1) write(*,*) 'Match found of non crust or OA aerosol CAMx species: ', mspecunspaced(lold),
     &                      ' and PMCAMx species: ', newspclist(lnew)
                 do i = 1, nx
                    do j = 1, ny
                       aremisnew(i,j,lnew) = aremis(i,j,lold) * pm25splitfactor
                    enddo !j
                 enddo !i

c  fine crust
           elseif ( ((mspecunspaced(lold)(1:len-1).eq.'FPRM')
     &        .or.  (mspecunspaced(lold)(1:len-1).eq.'FCRS'))
     &        .and. (newspclist(lnew)(1:len2-1).eq.'CRST')
     &        .and. (newspclist(lnew)(1:len2+1).ne.'CRST_7')
     &        .and. (newspclist(lnew)(1:len2+1).ne.'CRST_8') ) then
              if (t.eq.1) write(*,*) 'Match found of fine crustal CAMx species: ', mspecunspaced(lold),
     &                   ' and PMCAMx species: ', newspclist(lnew)
                 do i = 1, nx
                    do j = 1, ny               
                       aremisnew(i,j,lnew) = aremisnew(i,j,lnew) + aremis(i,j,lold) * pm25splitfactor
                    enddo !j
                 enddo !i

c  coarse crust
           elseif  ( ((mspecunspaced(lold)(1:len-1).eq.'CPRM')
     &        .or.  (mspecunspaced(lold)(1:len-1).eq.'CCRS'))
     &        .and. ((newspclist(lnew)(1:len2+1).eq.'CRST_7')
     &        .or. (newspclist(lnew)(1:len2+1).eq.'CRST_8')) ) then
             if (t.eq.1) write(*,*) 'Match found of coarse crustal CAMx species: ', mspecunspaced(lold),
     &                      ' and PMCAMx species: ', newspclist(lnew)
                 do i = 1, nx
                    do j = 1, ny
                       aremisnew(i,j,lnew) = aremisnew(i,j,lnew) + aremis(i,j,lold) * pmcoarsesplitfactor
                    enddo !j
                 enddo !i

c  aerosol organics
           elseif ( (mspecunspaced(lold)(1:len-1).eq.'POA')
     &        .and. (newspclist(lnew)(1:3).eq.'APO' ) ) then
              if (t.eq.1) write(*,*) 'Match found of OA CAMx species: ', mspecunspaced(lold),
     &                   ' and APO PMCAMx species: ', newspclist(lnew)
              if (newspclist(lnew)(1:4).eq.'APO1') then
                 apofactor = apofactorlist(1)
              elseif (newspclist(lnew)(1:4).eq.'APO2') then
                 apofactor = apofactorlist(2)
              elseif (newspclist(lnew)(1:4).eq.'APO3') then
                 apofactor = apofactorlist(3)
              elseif (newspclist(lnew)(1:4).eq.'APO4') then
                 apofactor = apofactorlist(4)
              endif !apo
              do i = 1, nx
                 do j = 1, ny
                    aremisnew(i,j,lnew) = aremis(i,j,lold) * pm25splitfactor * apofactor
                 enddo !j
              enddo !i

c  volatilized organics
           elseif ( (mspecunspaced(lold)(1:len-1).eq.'POA')
     &        .and. (newspclist(lnew)(1:3).eq.'CPO' ) ) then
              if (t.eq.1) write(*,*) 'Match found of OA CAMx species: ', mspecunspaced(lold),
     &                   ' and CPO PMCAMx species: ', newspclist(lnew)
              if (newspclist(lnew)(1:4).eq.'CPO5') then
                 cpofactor = cpofactorlist(5)
              elseif (newspclist(lnew)(1:4).eq.'CPO6') then
                 cpofactor = cpofactorlist(6)
              endif !cpo
              do i = 1, nx
                 do j = 1, ny
                    aremisnew(i,j,lnew) = aremis(i,j,lold) * 1./MW * cpofactor
                 enddo !j
              enddo !i
              
c non-traditional secondary
           elseif ( (mspecunspaced(lold)(1:len-1).eq.'POA')
     &        .and. (newspclist(lnew)(1:3).eq.'CNS' ) ) then
              if (t.eq.1) write(*,*) 'Match found of OA CAMx species: ', mspecunspaced(lold),
     &                   ' and CNS PMCAMx species: ', newspclist(lnew)  
              if (newspclist(lnew)(1:4).eq.'CNS6') then
                 cnsfactor = cnsfactorlist(6)
              elseif (newspclist(lnew)(1:4).eq.'CNS7') then
                 cnsfactor = cnsfactorlist(7)
              elseif (newspclist(lnew)(1:4).eq.'CNS8') then
                 cnsfactor = cnsfactorlist(8)
              endif !cns
              do i = 1, nx
                 do j = 1, ny
                    aremisnew(i,j,lnew) = aremis(i,j,lold) * 1./MW * cnsfactor
                 enddo !j
              enddo !i

           endif

         enddo !lold
      enddo !lnew
        

c Check for negative concentrations
      do l = 1, nspecnew
         do i = 1, nx
            do j = 1, ny
               if (aremisnew(i,j,l).lt.0.) then
                  write(*,*), 'aremisnew = ', aremisnew(i,j,l)
                  write(*,*) 'Negative Concentration at: i= ',i, 'j= ',j,'l = ', l
               endif
            enddo !j
         enddo !i
      enddo !l


c Write to output files
       do l = 1, nspecnew
          write(ifout) ione, (mspecnew(n,l),n=1,10),
     &    ((aremisnew(i,j,l),i=1,nx),j=1,ny)
       enddo !l

      goto 100

 900  write(6,*)'END of INPUT FILE.'
      goto 990
 990  close(ifin)
      if(ifout.ne.6) close(ifout)
      enddo !date
 999  continue

      end
