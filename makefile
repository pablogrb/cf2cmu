# Makefile for cf2cmu
FC = ifort

# FLAGS = -g -mieee-fp -align dcommons -convert big_endian -static_intel
# FLAGS = -O2 -mieee-fp -align dcommons -convert big_endian -static_intel
# FLAGS = -g -openmp -mieee-fp -align dcommons -convert big_endian -static_intel
FLAGS = -O3 -openmp -mieee-fp -align dcommons -convert big_endian -static_intel

TARGET = cf2cmu

MODULES = \
class_uam_iv.o
PROGRAMS = \
cf2cmu.o

OBJECTS = $(MODULES) $(PROGRAMS)

cf2cmu: $(OBJECTS)
	$(FC) $^ -o $@ $(FLAGS)

%.mod: %.f90
	$(FC) -c $^ $(FLAGS)

%.o: %.f90
	$(FC) -c $^ $(FLAGS)

.PHONY: clean

clean:
	rm -f $(OBJECTS) *.mod
