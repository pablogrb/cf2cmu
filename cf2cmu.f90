PROGRAM cf2cmu

USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Applies a linear transformation matrix to the species axis of a CAMx / PMCAMx UAM-IV
!		emissions file of ftype 'EMISSIONS ' or 'PTSOURCE  '
!	Inputs:
! 		Input inventory file name
! 		Output inventory file name
!		Conversion matrix file name
!	Outputs:
!		UAM-IV inventory file with new species
!	By:
!		Pablo Garcia
!		08-2017
!	NOTE:
!

!	------------------------------------------------------------------------------------------
!	Declarations

! 	Data type modules
	TYPE(UAM_IV) :: fl_inp							! Current input average file
	TYPE(UAM_IV) :: fl_out							! Output average file
	CHARACTER(LEN=256) :: inp_file, out_file		! File names

!	Conversion array
	CHARACTER(LEN=265) :: mat_file					! Matrix file name
	INTEGER :: mat_unit								! Matrix file unit
	INTEGER :: n_out_spec							! Number of output species
	CHARACTER(LEN=10), ALLOCATABLE :: s_inp_spec(:)	! Species array of the input UAM_IV file
	CHARACTER(LEN=10), ALLOCATABLE :: s_out_spec(:)	! Species array of the output

	REAL, ALLOCATABLE :: conv_matrix(:,:)			! Linear transformation matrix for species

!	Argument control
	INTEGER :: arg_num
	CHARACTER(LEN=2) :: arg_switch
	LOGICAL :: file_exists

!	Namelist IO
	CHARACTER(LEN=256) :: ctrlfile					! Control file path
	INTEGER :: nml_unit
	NAMELIST /FILE_IO/ inp_file, out_file, mat_file

!	Counters
	INTEGER :: i_sp_o, i_sp_i
	INTEGER :: i_nx, i_ny, i_hr, i_stk
	INTEGER :: i
	INTEGER :: pct_10

!	Status indicators
	INTEGER :: io_stat

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	Command line argument capture
	arg_num = COMMAND_ARGUMENT_COUNT()
	IF (arg_num .EQ. 0) THEN
		ctrlfile = 'cf2cmu.in'
		WRITE(*,*) 'Using the control file ', TRIM(ctrlfile)
	ELSEIF (arg_num .NE. 2) THEN
		WRITE(*,*) 'Bad argument number'
		CALL EXIT(0)
	ELSE
!		Capture the argument type
		CALL GET_COMMAND_ARGUMENT(1,arg_switch)
		IF (arg_switch .NE. '-f') THEN
			WRITE(*,*) 'Bad argument type'
			CALL EXIT(0)
		ELSE
			CALL GET_COMMAND_ARGUMENT(2,ctrlfile)
			WRITE(*,*) 'Using the control file ', TRIM(ctrlfile)
		END IF
	END IF

!	Read the namelist
	OPEN(NEWUNIT=nml_unit, FILE=ctrlfile, FORM='FORMATTED', STATUS='OLD', ACTION='READ')
	READ(nml_unit,NML=FILE_IO)
	CLOSE(nml_unit)

!	Check the paths
	INQUIRE(FILE=TRIM(inp_file), EXIST=file_exists)
	IF (.NOT. file_exists) THEN
		WRITE(*,*) 'Input file ', TRIM(inp_file), ' does not exist'
		CALL EXIT(1)
	END IF
	INQUIRE(FILE=TRIM(mat_file), EXIST=file_exists)
	IF (.NOT. file_exists) THEN
		WRITE(*,*) 'Matrix file ', TRIM(mat_file), ' does not exist'
		CALL EXIT(1)
	END IF
	INQUIRE(FILE=TRIM(out_file), EXIST=file_exists)
	IF (file_exists) THEN
		WRITE(*,*) 'Output file ', TRIM(inp_file), ' exists'
		WRITE(*,*) 'will not overwrite'
		CALL EXIT(1)
	END IF


!	------------------------------------------------------------------------------------------
!	Check the file type
	CALL inquire_header(fl_inp,inp_file)
!	Check for file type
	! IF (fl_inp%ftype .NE. 'EMISSIONS') THEN
	IF (.NOT. (fl_inp%ftype .EQ. 'EMISSIONS ' .OR. fl_inp%ftype .EQ. 'PTSOURCE  ')) THEN
		WRITE(0,*) 'Not a valid file type'
		CALL EXIT(0)
	END IF
!	Open the file
	CALL read_uamfile(fl_inp,inp_file)

!	------------------------------------------------------------------------------------------
!	Read the species matrix part 1
	OPEN(NEWUNIT=mat_unit,FILE=TRIM(mat_file),STATUS='OLD')
!	Read the number of out species.
	READ (mat_unit,*) n_out_spec
	WRITE(*,*) 'Number of output species:', n_out_spec
	REWIND mat_unit

! 	Allocate the vectors and arrays
	ALLOCATE(s_inp_spec(fl_inp%nspec))
	ALLOCATE(s_out_spec(n_out_spec))
	ALLOCATE(conv_matrix(fl_inp%nspec,n_out_spec))
! 	Read the out species list
	READ (mat_unit,*,IOSTAT=io_stat) n_out_spec, (s_out_spec(i_sp_o), i_sp_o=1,n_out_spec)
	IF (io_stat /= 0) THEN
		WRITE(0,*) 'The output columns value must match the number of columns in the matrix file'
		CALL EXIT(1)
	END IF

! 	Read the matrix
	DO i_sp_i = 1, fl_inp%nspec
		READ (mat_unit,*,IOSTAT=io_stat) s_inp_spec(i_sp_i), (conv_matrix(i_sp_i,i_sp_o), i_sp_o=1,n_out_spec)
		IF (io_stat < 0) THEN
			WRITE(0,*) 'The number of rows in the matrix must match the number of species in the input file'
			WRITE(*,*) 'The current input file species list is'
			DO i = 1, fl_inp%nspec
				WRITE(*,*) fl_inp%c_spname(i)
			END DO
			CALL EXIT(1)
		END IF
	END DO

	! Check if the inout species lists match
	IF (.NOT. ALL(fl_inp%c_spname .EQ. s_inp_spec)) THEN
		WRITE(*,*) 'The input species list of the matrix file must match the input file'
		WRITE(*,*) 'The current input file species list is'
		DO i_sp_i = 1, fl_inp%nspec
			WRITE(*,*) fl_inp%c_spname(i_sp_i)
		END DO
		CALL EXIT(1)
	END IF

!	------------------------------------------------------------------------------------------
!	Build the output file
!	------------------------------------------------------------------------------------------
!	Clone the files
	fl_out = fl_inp
! !	Clone the header
! 	CALL clone_header(fl_inp, fl_out)

!	Build the species list
	DEALLOCATE(fl_out%spname, fl_out%c_spname)
	ALLOCATE(fl_out%spname(10,n_out_spec))
	ALLOCATE(fl_out%c_spname(n_out_spec))
	DO i_sp_o = 1, n_out_spec
		DO i = 1,10
			fl_out%spname(i,i_sp_o) = s_out_spec(i_sp_o)(i:i)
		END DO
	END DO
	fl_out%c_spname = s_out_spec
	fl_out%nspec = n_out_spec
! ! 	Allocate the time headers
! 	ALLOCATE(fl_out%ibgdat(fl_inp%update_times),fl_out%iendat(fl_inp%update_times))
! 	ALLOCATE(fl_out%nbgtim(fl_inp%update_times),fl_out%nentim(fl_inp%update_times))
! !	Clone the time variant header arrays
! 	fl_out%ibgdat = fl_inp%ibgdat
! 	fl_out%iendat = fl_inp%iendat
! 	fl_out%nbgtim = fl_inp%nbgtim
! 	fl_out%nentim = fl_inp%nentim

	SELECT CASE (fl_inp%ftype)
	CASE ('EMISSIONS ')
!		Allocate the 2D emiss arrays
		DEALLOCATE(fl_out%aemis)
		ALLOCATE(fl_out%aemis(fl_out%nx, fl_out%ny, fl_out%update_times, fl_out%nspec))
	CASE ('PTSOURCE  ')
		! Allocate the stack emissions array
		DEALLOCATE(fl_out%ptemis)
		ALLOCATE(fl_out%ptemis(fl_out%update_times,fl_out%nstk,fl_out%nspec))
	END SELECT

!	------------------------------------------------------------------------------------------
!	Calculate the new emissions
!	------------------------------------------------------------------------------------------
	SELECT CASE (fl_inp%ftype)

!	------------------------------------------------------------------------------------------
	! 2D Emissions AKA Area emissions files
	CASE ('EMISSIONS ')
		WRITE(*,*) 'Converting the species'
		WRITE(*,'(I3,A)') 0, "% done"
!		Start parallel section
!$OMP 	PARALLEL SHARED(fl_inp, fl_out)

		! Startup the sanity display
		pct_10 = 1+fl_inp%nx/10

		!Loop through the rows, columns and hours
!$OMP 	DO SCHEDULE(DYNAMIC)
		DO i_nx = 1,fl_out%nx
			DO i_ny = 1,fl_out%ny
				DO i_hr = 1,fl_out%update_times
					fl_out%aemis(i_nx, i_ny, i_hr,:) = &
						& MATMUL(fl_inp%aemis(i_nx, i_ny, i_hr,:),conv_matrix)
				END DO
			END DO
!			Sanity display
			IF (MOD(i_nx,pct_10) .EQ. 0) THEN
				WRITE(*,'(I3,A)') 100*i_nx/fl_inp%nx, "% done"
			END IF
		END DO

!$OMP 	END DO NOWAIT
!		End of the parallel section
!$OMP 	END PARALLEL

!	------------------------------------------------------------------------------------------
	! Point source emissions
	CASE ('PTSOURCE  ')
		WRITE(*,*) 'Converting the species'
		WRITE(*,'(I3,A)') 0, "% done"
!		Start parallel section
!$OMP 	PARALLEL SHARED(fl_inp, fl_out)

		! Startup the sanity display
		pct_10 = 1+fl_inp%update_times/10

		! Loop through the hours and stacks
		! This parallelization scheme only scales up to fl_out%update_times threads
		! This value is normally 24
!$OMP 	DO SCHEDULE(DYNAMIC)
		DO i_hr = 1,fl_out%update_times
			DO i_stk = 1, fl_out%nstk
				fl_out%ptemis(i_hr,i_stk,:) = &
					& MATMUL(fl_inp%ptemis(i_hr,i_stk,:),conv_matrix)
			END DO
			! Sanity display
			IF (MOD(i_nx,pct_10) .EQ. 0) THEN
				WRITE(*,'(I3,A)') 100*i_hr/fl_inp%update_times, "% done"
			END IF
		END DO

!$OMP 	END DO NOWAIT
!		End of the parallel section
!$OMP 	END PARALLEL

	END SELECT

!	Write the ouput file
	CALL write_uamfile(fl_out,out_file)

END PROGRAM cf2cmu
